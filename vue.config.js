// 这是一个webpack的配置文件  如果和基础配置重复
const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
    transpileDependencies: true,
    lintOnSave: false,
    chainWebpack: (config) => {
        config.module
            .rule('less')
            .test(/\.less$/)
            .oneOf('vue')
            .use('px2rem-loader')
            .loader('px2rem-loader')
            .before('postcss-loader') // this makes it work.
            .options({
                remUnit: 75, //根据视觉稿，rem为px的⼗分之⼀，750px  75rem
                remPrecision: 8 //保留8位⼩数
            })
            .end();
    }
});
