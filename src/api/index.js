// 所有请求的对外抛出

// 首页模块接口
import HomeApi from './home';
import myInfo from './myInfo';
export default {
    ...HomeApi,
    ...myInfo
};
