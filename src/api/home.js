// 首页所有的请求接口在这里统一处理:高盼盼
import axios from '../utils/http';
const HomeApi = {
    // 首页数据汇总
    getDiyDefault() {
        return axios.get(`/v2/diy/get_diy/default`);
    },
    // 促销精品数据
    getIndex() {
        return axios.get(`/v2/index`);
    },
    getSwiper() {
        return axios.get(`/v2/index`);
    }

    // 其他接口…………   40个
};

export default HomeApi;
