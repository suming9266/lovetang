// 个人中心所有的请求接口在这里统一处理:周源鑫
// 首页所有的请求接口在这里统一处理:高盼盼
import axios from '../utils/http';
const MyInfoApi = {
    // 首页数据汇总
    getMyInfoDefault() {
        return axios.get(`/v2/diy/get_diy/default`);
    },
    // 促销精品数据
    getMyInfoIndex() {
        return axios.get(`/v2/index`);
    }
// 紧急修改的任务
    // 其他接口…………   30个
};

export default MyInfoApi;
