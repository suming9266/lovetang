import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import 'amfe-flexible';
// 导入全局过滤器
import filters from './filters';
// 初始化浏览器的标准
import './assets/reset.css';
import apiMap from '@/api';
import './utils/importVant';

// 士大夫撒旦法

// 我是苏明写的功能
// 是为了测试冲突解决的
// 使用ssh方式提交
// 这是我今天自己写的过滤器功能
// 进行的导入 
// 循环遍历变成全局的过滤器
// 把项目这种所有的接口挂载到vue的原型链上
console.log('filters', Object.keys(filters));
for (let key of Object.keys(filters)) {
    Vue.filter(key, filters[key]);
}

Vue.prototype.$api = apiMap;
Vue.config.productionTip = false;
new Vue({
    router,
    store,
    render: (h) => h(App)
}).$mount('#app');
