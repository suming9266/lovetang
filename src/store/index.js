import Vue from 'vue';
import Vuex from 'vuex';
import user from './modules/user';
import listData from './modules/listData';
// 导入购物车模块
import shopCar from './modules/shopcar';
// 第一步，引入持久化插件
// import createPersistedState from 'vuex-persistedstate';
Vue.use(Vuex);

export default new Vuex.Store({
    // strict: true,
    // 模块化.便于统一管理
    modules: { user, listData, shopCar }
    //第二步，进行持久化的配置
    // plugins: [
    //     createPersistedState({
    //         // 这里可以配置存储在localStorage还是sessionStorage, 默认是localStorage,例如
    //         storage: window.sessionStorage,
    //         // paths是持久化存储state中的哪些数据，如果是模块下具体的数据需要加上模块名称，如payment.amount
    //         paths: ['shopCar']
    //     })
    // ]
});
