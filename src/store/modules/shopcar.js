// 购物车模块存储共享数据

const state = {
    productList: [
        { title: '手机', price: '1399', logo: 'opple', color: '黑色' },
        { title: '键盘', price: '999', logo: '雷蛇', color: '黑色' }
    ]
};
// 可以对共享的数据进行提前处理的,相当于是外面的computed计算属性
const getters = {
    orderPriceProd(state) {
        return state.productList.sort((a, b) => {
            return a.price * 1 - b.price * 1;
        });
    }
};
// 唯一可以显式                同步的方式修改state的地方
// 如果这里面有异步的存在.首先如果开了严格模式.直接报错.即使没有开严格模式.也不利于
// vue-devtools这种工具对状态变换的追踪
const mutations = {
    SAVETOSTATE(state, params) {
        state.productList.push(params);
        // sessionStorage.setItem(
        //     'productList',
        //     JSON.stringify(state.productList)
        // );
    }
};
// 里面是异步的在组件中通过dispatch触发这里面的方法.里面通过commit触发mutation的方法
const actions = {
    saveProd({ commit }, params) {
        // setTimeout(() => {
        console.log('在vuex中', params);
        // 触发mutations里面的方法
        commit('SAVETOSTATE', params);
        // }, 10);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
