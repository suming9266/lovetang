// 存放共享状态的地方.状态改变.各组件中使用状态的地方都会被改变
const state = {
    userInfo: {}
};
// 相当于一个计算属性,可以在外面获取state值之前做一些处理
const getters = {};
// 同步的方式来修改state中的状态
const mutations = {};
// 异步的,可以运行异步代码的存在.通过commit触发mutations中的代码
const actions = {
    otherAction() {
        console.log('触发otherAction');
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
