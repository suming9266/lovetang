// 存放共享状态的地方.状态改变.各组件中使用状态的地方都会被改变
const state = {
    userInfo: {},
    Listdata: [
        {
            news: '俄乌战场实况.....'
        }
    ]
};
// 相当于一个计算属性,可以在外面获取state值之前做一些处理
const getters = {
    ListdataNow(state) {
        return state.Listdata[0].news + '演示';
    }
};
// 同步的方式来修改state中的状态
const mutations = {
    saveMutation(state, params) {
        state.userInfo = params;
    }
};
// 异步的,可以运行异步代码的存在.通过commit触发mutations中的代码
const actions = {
    saveUserInfo({ commit, dispatch }, params) {
        // dispatch('otherAction');
        dispatch('listData/otherAction', params, { root: true });
        console.log('触发我', params);
        commit('saveMutation', params);
    }
};

export default {
    // 命名空间
    namespaced: true,
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions
};
