import Vue from 'vue';
import VueRouter from 'vue-router';
import HomeView from '../views/HomeView.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView
    },
    {
        path: '/set',
        name: 'set',

        component: () =>
            import(/* webpackChunkName: "set" */ '../views/Set.vue')
    },
    {
        path: '/vuex',
        name: 'vuex',

        component: () =>
            import(/* webpackChunkName: "set" */ '../views/ProductList.vue')
    },
    {
        path: '/watch',
        name: 'set',

        component: () =>
            import(/* webpackChunkName: "set" */ '../views/computedWatch.vue')
    },
    {
        path: '/about',
        name: 'about',
        component: () =>
            import(/* webpackChunkName: "set" */ '../views/AboutView.vue'),
        children: [
            {
                path: '/tab1',
                name: 'home',
                component: HomeView
            }
        ]
        // beforeEnter: () => {
        //     alert(1111);
        // }
    }
];

const router = new VueRouter({
    mode: 'hash',
    base: process.env.BASE_URL,
    routes
});

// 路由的全局导航守卫钩子
// 所有的路由跳转都走这里
router.beforeEach((to, from, next) => {
    next();
});
router.afterEach();

export default router;
