import Vue from 'vue';

import { Swipe, SwipeItem } from 'vant';
import { Button } from 'vant';
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Button);
